// 1) Задание
// Типы данных бывают: числовые (number), строковые (string), булевые ('Boolean'), Null, Undefined,
//  BigInt, infinity, объекты (object). Так же еще есть объектнные типы данных: масив, объект, функции.

// 2) Задание
// Операторы == и === обозначают строгое и не строгое равенство, разница между ними == сравнивает по значению,
// а === по значению и по типу.

//  2) Задание
//  Операторы нам помогают исполнять код, один или другой участок кода при определенных условиях.

let userName = prompt("What is your name", );
let userAge = +prompt("How old are you", );

// let dateUser = userName + userAge;
// console.log(`Имя пользователяn  ${userName}  Возраст пользователя  ${userAge}`);

while (
 
    userName === "" ||
    userName === null ||
    userAge === "" ||
    userAge === null ||
    userAge === 0 ||
    userAge === NaN
) {
  userName = prompt("Please enter your name", userName);
  userAge = +prompt("Please enter your age", userAge);
}

if (userAge < 18) {
  alert("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
  let welcome = confirm("Are you sure you want to continue?");

  if (welcome === true) {
    alert("Welcome " + `${userName}`);
  } else {
    alert("You are not allowed to visit this website");
  }
} else {
  alert(`Welcome   ${userName}`);
}
